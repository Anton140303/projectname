<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ArticleType
 *
 * @package App\Form
 */
class ArticleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Название: ',
                'attr' => [
                    'placeholder' => 'Введите название здесь...'
                ]
            ])
            ->add('description', TextType::class, [
                'label' => 'Описание: ',
                'attr' => [
                    'placeholder' => 'Введите описание...',
                ]
            ])
            ->add('created_at', DateTimeType::class, [
                'label' => 'Дата создания: '
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Сохранить'
            ])
            ->add('reset', ResetType::class, [
                'label' => 'Сбросить'
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class
        ]);
    }
}